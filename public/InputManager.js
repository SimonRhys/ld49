var InputManager = (function() {

   var CANVAS = document.getElementById("gl-canvas");
   var MOUSE_LOCKED = false;
   var MOUSE_POS = {x: 0, y: 0};
   var ROTATE_SPEED = 1;

   var currentlyPressedKeys = {};
   var controllerInput = false;

   var controls = {
      MOVE_FORWARD: "MoveForward",
      MOVE_BACKWARD: "MoveBackward",
      MOVE_LEFT: "MoveLeft",
      MOVE_RIGHT: "MoveRight",
      ROTATE_LEFT: "RotateLeft",
      ROTATE_RIGHT: "RotateRight",
      INTERACT: "Interact"
   }

   var moveForwardKeyCode = "W".charCodeAt(0);
   var moveBackwardKeyCode = "S".charCodeAt(0);
   var moveLeftKeyCode = "A".charCodeAt(0);
   var moveRightKeyCode = "D".charCodeAt(0);

   var rotateLeftKeyCode = "O".charCodeAt(0);
   var rotateRightKeyCode = "P".charCodeAt(0);

   var interactKeyCode = "E".charCodeAt(0);
   var kickKeyCode =  " ".charCodeAt(0);

   function IsKeyPressed(key) {
      var keyCode = key;

      if (typeof key == "string") keyCode = key.charCodeAt(0);

      return currentlyPressedKeys[keyCode];
   }


   function IsMovingForward() {
      return currentlyPressedKeys[moveForwardKeyCode] || controllerInput;
   }


   function IsMovingBackward() {
      return currentlyPressedKeys[moveBackwardKeyCode] || controllerInput;
   }


   function IsMovingLeft() {
      return currentlyPressedKeys[moveLeftKeyCode] || controllerInput;
   }


   function IsMovingRight() {
      return currentlyPressedKeys[moveRightKeyCode] || controllerInput;
   }


   function IsRotatingLeft() {
      return currentlyPressedKeys[rotateLeftKeyCode] || controllerInput;
   }


   function IsRotatingRight() {
      return currentlyPressedKeys[rotateRightKeyCode] || controllerInput;
   }

   function IsKicking() {
      return currentlyPressedKeys[kickKeyCode] || controllerInput;
   }

   function Interact() {
      return currentlyPressedKeys[interactKeyCode] || controllerInput;
   }


   function CustomiseControls(control, keyCode) {
      if (control == controls.MOVE_FORWARD) moveForwardKeyCode = keyCode;
      if (control == controls.MOVE_BACKWARD) moveBackwardKeyCode = keyCode;
      if (control == controls.MOVE_LEFT) moveLeftKeyCode = keyCode;
      if (control == controls.MOVE_RIGHT) moveRightKeyCode = keyCode;

      if (control == controls.ROTATE_LEFT) rotateLeftKeyCode = keyCode;
      if (control == controls.ROTATE_RIGHT) rotateRightKeyCode = keyCode;

      if (control == controls.INTERACT) interactKeyCode = keyCode;
   }


   function HandleKeyDown(event) {
      currentlyPressedKeys[event.keyCode] = true;
   }


   function HandleKeyUp(event) {
      currentlyPressedKeys[event.keyCode] = false;
   }


   function HandleMouseMove(event) {
      var move = { x: event.movementX || event.mozMovementX || 0, 
         y: event.movementY || event.mozMovementY || 0};

         // POSITION.yaw += move.x/(ROTATE_SPEED*100);
         // POSITION.pitch += move.y/(ROTATE_SPEED*100);
     
         // if(Math.abs(POSITION.pitch) > 1.28539816339) {
         //     POSITION.pitch = 1.28539816339*Math.sign(POSITION.pitch);
         // }
   }


   function SetMousePosition(position) {
      MOUSE_POS.x = position.x - CANVAS.clientWidth;
      MOUSE_POS.y = position.y - CANVAS.clientHeight;
   }


   function HandlePointerLock() {
      if(document.pointerLockElement === CANVAS || document.mozPointerLockElement === CANVAS) {
            if(MOUSE_LOCKED) return;

            console.log('The pointer lock status is now locked, looking around enabled.'); 
            document.addEventListener("mousemove", HandleMouseMove, false);
            SetMousePosition(CANVAS.width / 2, CANVAS.height / 2);
            MOUSE_LOCKED = true;
      } else {
         console.log('The pointer lock status is now unlocked, looking around disabled.');  
         document.removeEventListener("mousemove", HandleMouseMove, false);
         MOUSE_LOCKED = false;
      }
  }

   return {
      IsKeyPressed: IsKeyPressed,
      IsMovingForward: IsMovingForward,
      IsMovingBackward: IsMovingBackward,
      IsMovingLeft: IsMovingLeft,
      IsMovingRight: IsMovingRight,
      IsRotatingLeft: IsRotatingLeft,
      IsRotatingRight: IsRotatingRight,
      IsKicking: IsKicking,
      Interact: Interact,

      CustomiseControls: CustomiseControls,
      Controls: controls,

      HandleKeyDown: HandleKeyDown,
      HandleKeyUp: HandleKeyUp,
      HandleMouseMove: HandleMouseMove,
      HandlePointerLock: HandlePointerLock,
   }
})();