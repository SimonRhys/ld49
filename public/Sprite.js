var Sprite = function(x=0, y=0, w=32, h=32) {

   var gl = null;
   var vao;
   var vbo;
   var texVBO;

   var pos = {x: x, y: y, z: 0};
   var size = {x: w, y: h};

   var colour = {r: 0, g: 0, b: 0, a: 1};
   var texture;
   var animation;
   var texID;
   var textureScale = 1;
   var hasTexture = false;

   function InitGL(webgl) {
      gl = webgl;

      vao = gl.createVertexArray();
      vbo = gl.createBuffer();
      texVBO = gl.createBuffer();

      UpdateVertexData();
   }

   function Draw(shader) {
      gl.bindVertexArray(vao);

      if (hasTexture) {
         gl.activeTexture(gl.TEXTURE0);
         gl.bindTexture(gl.TEXTURE_2D, texID);
         shader.Uniform1i("tex", 0);
      }

      gl.drawArrays(gl.TRIANGLES, 0, 6);

      gl.bindVertexArray(null);
   }

   function GetPosition() {
      return pos;
   }

   function GetSize() {
      return size;
   }

   function GetColour() {
      return colour;
   }

   function SetPosition(x, y, z) {
      pos.x = x;
      pos.y = y;
      pos.z = z;

      UpdateVertexData();
   }

   function SetSize(w, h) {
      size.x = w;
      size.y = h;
      
      UpdateVertexData();
   }

   function SetColour(c) {
      colour = c;
   }

   function SetTexture(tex) {
      texture = tex;
      texID = tex.GetID();
      hasTexture = true;
   }
   
   function SetTextureID(id) {
      texID = id;
      hasTexture = true;
   }

   function SetTextureScale(scale) {
      textureScale = scale;

      if (hasTexture) {
         gl.bindTexture(gl.TEXTURE_2D, texID);
         gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
         gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
      }

      UpdateVertexData();
   }
   
   function SetAnimation(anim) {
      animation = anim;
   }

   function Update(dt) {

      if (animation) {
         animation.Update(dt);
         texture = animation.GetFrame();
         texID = texture.GetID();
      }
   }

   function UpdateVertexData() {
      gl.bindVertexArray(vao);

      var x1 = pos.x;
      var y1 = pos.y;
      var x2 = pos.x + size.x;
      var y2 = pos.y + size.y;

      var verts = [
         x1, y1, 0,
         x2, y1, 0,
         x2, y2, 0,

         x2, y2, 0,
         x1, y2, 0,
         x1, y1, 0,
      ];

      var texVerts = [
         0, 0,
         textureScale, 0,
         textureScale, textureScale,

         textureScale, textureScale,
         0, textureScale,
         0, 0,
      ];

      gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verts), gl.STATIC_DRAW);
      gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 0, 0);
      gl.enableVertexAttribArray(0);

      gl.bindBuffer(gl.ARRAY_BUFFER, texVBO);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(texVerts), gl.STATIC_DRAW);
      gl.vertexAttribPointer(1, 2, gl.FLOAT, false, 0, 0);
      gl.enableVertexAttribArray(1);

      gl.bindVertexArray(null);
   }

   return {
      InitGL: InitGL,
      Draw: Draw,
      GetPosition: GetPosition,
      GetSize: GetSize,
      GetColour: GetColour,
      SetPosition: SetPosition,
      SetColour: SetColour,
      SetAnimation: SetAnimation,
      SetSize: SetSize,
      SetTexture: SetTexture,
      SetTextureID: SetTextureID,
      SetTextureScale: SetTextureScale,
      Update: Update,
   }

}