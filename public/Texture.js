var Texture = function() {

   var gl;

   var texID = -1;
   var size = { x: 0, y: 0 };

   function InitGL(webgl) {
      gl = webgl;
   }

   function GetID() {
      return texID;
   }

   function GetSize() {
      return size;
   }

   async function LoadTexture(fileName) {
      if (texID < 0) texID = gl.createTexture();

      var image = await LoadImage(fileName);

      HandleTextureLoaded(image);
  
      return texID;
   }


   function LoadImage(fileName) {
      return new Promise((resolve) => {
         var image = new Image();
         image.onload = () => { resolve(image) };
         image.src = fileName;
      });
   }
  

   function HandleTextureLoaded(image) {
      gl.bindTexture(gl.TEXTURE_2D, texID);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);

      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      
      gl.generateMipmap(gl.TEXTURE_2D);
      gl.bindTexture(gl.TEXTURE_2D, null);

      size.x = image.width;
      size.y = image.height;
   }
  
  return {
     InitGL: InitGL,
     GetID: GetID,
     GetSize: GetSize,
     LoadTexture: LoadTexture,
  }
}