var InstancedSprite = function(x=0, y=0, w=32, h=32) {

   var gl = null;
   var vao;
   var vbo;
   var texVBO;
   var instanceVBOs = [];
   var maxInstanceCount = 500;

   var pos = {x: x, y: y, z: 0};
   var size = {x: w, y: h};

   var colour = {r: 0, g: 0, b: 0, a: 1};
   var texture;
   var animation;
   var texID;
   var hasTexture = false;

   var instancePositions = [];
   var numSprites = 0;

   function InitGL(webgl) {
      gl = webgl;

      vao = gl.createVertexArray();
      vbo = gl.createBuffer();
      texVBO = gl.createBuffer();
      instanceVBO = gl.createBuffer();

      UpdateVertexData();
   }

   function Draw(shader) {
      gl.bindVertexArray(vao);

      if (hasTexture) {
         gl.activeTexture(gl.TEXTURE0);
         gl.bindTexture(gl.TEXTURE_2D, texID);
         shader.Uniform1i("tex", 0);
      }

      for (var i = 0; i < instanceVBOs.length; i++) {
         gl.bindBuffer(gl.ARRAY_BUFFER, instanceVBOs[i].buffer);
         gl.vertexAttribPointer(2, 3, gl.FLOAT, false, 0, 0);
         gl.vertexAttribDivisor(2, 1);
         gl.enableVertexAttribArray(2);
         gl.drawArraysInstanced(gl.TRIANGLES, 0, 6, instanceVBOs[i].count);
      }

      gl.bindVertexArray(null);
   }

   function GetPositions() {
      return instancePositions;
   }

   function GetSize() {
      return size;
   }

   function GetColour() {
      return colour;
   }

   function SetPositions(positions, spriteCount) {
      instancePositions = positions;
      numSprites = spriteCount;

      var offset = 0;
      while (offset < numSprites) {
         var length = Math.min(numSprites - offset, maxInstanceCount);

         var index = instanceVBOs.length;
         instanceVBOs[index] = { buffer: gl.createBuffer(), count: length };

         var bufferPositions = [];
         for (var i = 0; i < length * 3; i++) {
            bufferPositions[i] = positions[offset * 3 + i];
         }

         gl.bindBuffer(gl.ARRAY_BUFFER, instanceVBOs[index].buffer);
         gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(bufferPositions), gl.STATIC_DRAW);
         gl.vertexAttribPointer(2, 3, gl.FLOAT, false, 0, 0);
         gl.vertexAttribDivisor(2, 1);
         gl.enableVertexAttribArray(2);

         offset += length;
      }
   }

   function SetSize(w, h) {
      size.x = w;
      size.y = h;
      
      UpdateVertexData();
   }

   function SetColour(c) {
      colour = c;
   }

   function SetTexture(tex) {
      texture = tex;
      texID = tex.GetID();
      hasTexture = true;
   }
   
   function SetTextureID(id) {
      texID = id;
      hasTexture = true;
   }
   
   function SetAnimation(anim) {
      animation = anim;
   }

   function Update(dt) {

      if (animation) {
         animation.Update(dt);
         texture = animation.GetFrame();
         texID = texture.GetID();
      }
   }

   function UpdateVertexData() {
      gl.bindVertexArray(vao);

      var x1 = pos.x;
      var y1 = pos.y;
      var x2 = pos.x + size.x;
      var y2 = pos.y + size.y;

      var verts = [
         x1, y1, 0,
         x2, y1, 0,
         x2, y2, 0,

         x2, y2, 0,
         x1, y2, 0,
         x1, y1, 0,
      ];

      var texVerts = [
         0, 0,
         1, 0,
         1, 1,

         1, 1,
         0, 1,
         0, 0,
      ];

      gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verts), gl.STATIC_DRAW);
      gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 0, 0);
      gl.enableVertexAttribArray(0);

      gl.bindBuffer(gl.ARRAY_BUFFER, texVBO);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(texVerts), gl.STATIC_DRAW);
      gl.vertexAttribPointer(1, 2, gl.FLOAT, false, 0, 0);
      gl.enableVertexAttribArray(1);

      gl.bindVertexArray(null);
   }

   return {
      InitGL: InitGL,
      Draw: Draw,
      GetPositions: GetPositions,
      GetSize: GetSize,
      GetColour: GetColour,
      SetPositions: SetPositions,
      SetColour: SetColour,
      SetAnimation: SetAnimation,
      SetSize: SetSize,
      SetTexture: SetTexture,
      SetTextureID: SetTextureID,
      Update: Update,
   }

}