function Lerp(a, b, t) {
   return a  + t * (b - a);
}

var Vector3 = function(x1, y1, z1) {
   var x;
   var y;
   var z;

   if (x1 == undefined) x1 = 0;
   if (y1 == undefined) y1 = 0;
   if (z1 == undefined) z1 = 0;

   x = x1;
   y = y1;
   z = z1;

   return {
      x: x,
      y: y,
      z: z
   }
}


Vector3.Add = function(v1, v2) {
   return new Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

Vector3.Negate = function(v1, v2) {
   return new Vector3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

Vector3.Multiply = function(v1, s) {
   return new Vector3(v1.x * s, v1.y * s, v1.z * s);
}

Vector3.Dot = function(v1, v2) {
   return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}


Vector3.Cross = function(v1, v2) {
   var result = new Vector3();

   result.x = v1.y * v2.z - v1.z * v2.y;
   result.y = v1.x * v2.z - v1.z * v2.x;
   result.z = v1.x * v2.y - v1.y * v2.x;

   return result;
}


Vector3.Length = function(v1) {
   return Math.sqrt(Vector3.Dot(v1, v1));
}


Vector3.LengthSq = function(v1) {
   return Vector3.Dot(v1, v1);
}


Vector3.GetNormalised = function(v1) {
   var divisor = Vector3.Length(v1);
   var result = new Vector3(v1.x / divisor, v1.y / divisor, v1.z / divisor);

   return result;
}


Vector3.Lerp = function(v1, v2, t) {
   var result = new Vector3(0, 0, 0);

   result.x = Lerp(v1.x, v2.x, t);
   result.y= Lerp(v1.y, v2.y, t);
   result.z = Lerp(v1.z, v2.z, t);

   return result;
} 


var Quaternion = function(s, v) {
   var scalar = 0;
   var vector = new Vector3(0, 0, 0);

   if (s == undefined) s = 0;
   if (v == undefined) v = new Vector3(0, 0, 0);

   scalar = s;
   vector = new Vector3(v.x, v.y, v.z);

   return {
      scalar: scalar,
      vector: vector,
   }
}


Quaternion.Multiply = function (q1, q2) {
   var result = new Quaternion();

   result.scalar = q1.scalar * q2.scalar - Vector3.Dot(q1.vector, q2.vector);
   result.vector = Vector3.Cross(q1.vector, q2.vector);
   result.vector = Vector3.Add(result.vector, Vector3.Multiply(q1.vector, q2.scalar));
   result.vector = Vector3.Add(result.vector, Vector3.Multiply(q2.vector, q1.scalar));

   return result;
}


Quaternion.GetNormalised = function(q) {
   var result = new Quaternion();
   var n = Math.sqrt(
      q.vector.x * q.vector.x + 
      q.vector.y * q.vector.y + 
      q.vector.z * q.vector.z + 
      q.scalar * q.scalar);

   result.scalar = q.scalar / n;
   result.vector.x = q.vector.x / n;
   result.vector.y = q.vector.y / n;
   result.vector.z = q.vector.z / n;

   return result;
}


var Matrix4 = function() {
   
   var m = [
      [1, 0, 0, 0],
      [0, 1, 0, 0],
      [0, 0, 1, 0],
      [0, 0, 0, 1],
   ];

   function ToFloat32Array() {
      var flat = [];
      var count = 0;

      for (var i=0; i < 4; i++) {
         for (var j=0; j < 4; j++) {
            flat[count++] = m[i][j];
         }
      }

      return new Float32Array(flat);
   }


   return {
      m: m,
      ToFloat32Array: ToFloat32Array,
   }
}


Matrix4.GetPerspective = function(fovX, aspect, near, far) {

   var top = Math.tan(fovX * 0.5) * near;
   var bot = -top;
   var right = top * aspect;
   var left = -right;

   var mat = new Matrix4();

   mat.m[0][0] = 2 * near / (right - left);
   mat.m[0][1] = 0;
   mat.m[0][2] = 0;
   mat.m[0][3] = 0;

   mat.m[1][0] = 0;
   mat.m[1][1] = 2 * near / (top - bot);
   mat.m[1][2] = 0;
   mat.m[1][3] = 0;

   mat.m[2][0] = (right + left) / (right - left);
   mat.m[2][1] = (top + bot) / (top - bot);
   mat.m[2][2] = -(far + near) / (far - near);
   mat.m[2][3] = -1;

   mat.m[3][0] = 0;
   mat.m[3][1] = 0;
   mat.m[3][2] = (-2 * far * near) / (far - near);
   mat.m[3][3] = 0;

   return mat;
}


Matrix4.GetOrthographic = function(left, right, bot, top, near, far) {
   var mat = new Matrix4();

   mat.m[0][0] = 2 / (right - left);
   mat.m[0][1] = 0;
   mat.m[0][2] = 0;
   mat.m[0][3] = 0;

   mat.m[1][0] = 0;
   mat.m[1][1] = 2 / (top - bot);
   mat.m[1][2] = 0;
   mat.m[1][3] = 0;

   mat.m[2][0] = 0;
   mat.m[2][1] = 0;
   mat.m[2][2] = -2 / (far - near);
   mat.m[2][3] = 0;

   mat.m[3][0] = -((right + left) / (right - left));
   mat.m[3][1] = -((top + bot) / (top - bot));
   mat.m[3][2] = -((far + near) / (far - near));
   mat.m[3][3] = 1;

   return mat;
}

Matrix4.Translate = function(x, y, z) {
   var mat = new Matrix4();

   mat.m[3][0] = x;
   mat.m[3][1] = y;
   mat.m[3][2] = z;

   return mat;
}

Matrix4.Scale = function(x, y, z) {
   var mat = new Matrix4();

   mat.m[0][0] = x;
   mat.m[1][1] = y;
   mat.m[2][2] = z;

   return mat;
}

Matrix4.Rotate = function(xAngle, yAngle, zAngle) {
   var xQuaternion = new Quaternion(Math.cos(xAngle * 0.5), new Vector3(Math.sin(xAngle * 0.5), 0, 0));
   var yQuaternion = new Quaternion(Math.cos(yAngle * 0.5), new Vector3(0, Math.sin(yAngle * 0.5), 0));
   var zQuaternion = new Quaternion(Math.cos(zAngle * 0.5), new Vector3(0, 0, Math.sin(zAngle * 0.5)));

   var q = Quaternion.Multiply(xQuaternion, yQuaternion);
   q = Quaternion.Multiply(q, zQuaternion);
   q = Quaternion.GetNormalised(q);

   var s = q.scalar;
   var x = q.vector.x;
   var y = q.vector.y;
   var z = q.vector.z;
   var x2 = x * x;
   var y2 = y * y;
   var z2 = z * z;

   var mat = new Matrix4();

   mat.m[0][0] = 1 - 2 * (y2 + z2); 
   mat.m[0][1] = 2 * (x * y + z * s); 
   mat.m[0][2] = 2 * (x * z - y * s); 
   mat.m[0][3] = 0;

   mat.m[1][0] = 2 * (x * y - z * s);
   mat.m[1][1] = 1 - 2 * (x2 + z2);
   mat.m[1][2] = 2 * (y * z + x * s);
   mat.m[1][3] = 0;

   mat.m[2][0] = 2 * (x * z + y * s);
   mat.m[2][1] = 2 * (y * z - x * s);
   mat.m[2][2] = 1 - 2 * (x2 + y2);
   mat.m[2][3] = 0;

   mat.m[3][0] = 0;
   mat.m[3][1] = 0;
   mat.m[3][2] = 0;
   mat.m[3][3] = 1;

   return mat;
}

Matrix4.Multiply = function(a, b) {
   var mat = new Matrix4();

   for (var i=0; i < 4; i++) {
      for (var j=0; j < 4; j++) {
         mat.m[i][j] = 0;

         for (var k=0; k < 4; k++) {
            mat.m[i][j] += a.m[i][k] * b.m[k][j];
         }
      }
   }
   
   return mat;
}


Matrix4.Print = function(m) {
   for (var i = 0; i < 4; i++) {
      var line = "";

      for (var j = 0; j < 4; j++) {
         line += m.m[i][j] + ", ";
      }

      console.log(line);
   }
}