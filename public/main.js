var main = (function () {

//--------------------------------------------------------------------------
// Global Variables

var gl = null;

var CANVAS;

var PREVIOUS_FRAME = 0;

var CAMERA = {
   proj: new Matrix4(),
   orthoProj: new Matrix4(),
   view: new Matrix4(),
   yaw: 0,
   pitch: 0,
   xOffset: 0,
   yOffset: 0,
   zOffset: 0,
};

var PLAYER_POSITION = new Vector3(200, 250, 0);
var PLAYER_VELOCITY = 0;
var PLAYER_ACCELERATION = 0;
var PLAYER_ANGLE = -Math.PI * 0.5;
var PLAYER_DIRECTION = new Vector3(
   Math.cos(PLAYER_ANGLE),
   Math.sin(PLAYER_ANGLE),
   0);

var JERK = 500;
var ANGLE_SPEED = 2;
var MAX_VELOCITY = 300;
var FRICTION = 0.99;

{
   CAMERA.view = Matrix4.Translate(
      -PLAYER_POSITION.x,
      -PLAYER_POSITION.y,
      -PLAYER_POSITION.z);
}

var PLAYER_CAR = new Sprite(-32, -32, 64, 64);

var SPRITE_SHADER = new Shader();
var INSTANCED_SPRITE_SHADER = new Shader();

var CAR_TEX = new Texture();

var FADE_OUT_TIME = 4;
var FADE_OUT_TIMER = 0;

var CHANGE_TIME = 15;
var CHANGE_TIMER = CHANGE_TIME;

var POSSIBLE_KEYS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

var FORWARD_KEY = "W";
var BACKWARD_KEY = "S";
var LEFT_KEY = "A";
var RIGHT_KEY = "D";

var FORWARD_KEY_INDEX = 22;
var BACKWARD_KEY_INDEX = 18;
var LEFT_KEY_INDEX = 0;
var RIGHT_KEY_INDEX = 3;

var BACKGROUND;
var MAP = [];
var MAP_TILES = [];
var MAP_SPRITES = [];
var MAP_TEXTURES = [];

var CHECKPOINT_DISTANCE = 128;
var START_TILE = {x: 0, y: 0, worldX: 0, worldY: 0};
var TRACK = [];

var RACE_STARTED = false;
var COUNTDOWN_TIMER = 3;
var CURRENT_LAP = 0;
var MAX_LAPS = 3;
var BEST_RACE_TIMER = 0;
var CURRENT_RACE_TIMER = 0;


//--------------------------------------------------------------------------
// This function is called once the HTML page has finished loading. It sets
// up all the listening events for controls, sets up the WebGL, loads the
// required models and basically anything else needed before starting the
// animation loop.
//
async function Start() {
   CANVAS = document.getElementById("glcanvas", { alpha: false });
   CANVAS.oncontextmenu = function(e) {
      e.preventDefault();
   };
   
   if (!InitWebGL()) return;
   
   gl.clearColor(0.0, 0.0, 0.0, 1.0);
   gl.enable(gl.CULL_FACE);
   gl.enable(gl.DEPTH_TEST);

   gl.enable(gl.BLEND);
   gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

   gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
   gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
   
   window.addEventListener("resize", ResizeCanvas);
   
   // CANVAS.requestPointerLock = CANVAS.requestPointerLock ||
   // CANVAS.mozRequestPointerLock;

   // CANVAS.onclick = function() {
   //     CANVAS.requestPointerLock();
   // }

   document.onkeydown = InputManager.HandleKeyDown;
   document.onkeyup = InputManager.HandleKeyUp;
   // document.onmousemove = InputManager.HandleMouseMove;
   // document.addEventListener('pointerlockchange', InputManager.HandlePointerLock, false);
   // document.addEventListener('mozpointerlockchange', InputManager.HandlePointerLock, false);

   ResizeCanvas();
   
   // await  SoundManager.Load("Audio/ouch1.wav", "ouch");

   await LoadAllModels();


   //--------------------------------------------------------------------------
   // Setup shaders.

   SPRITE_SHADER.InitGL(gl);
   SPRITE_SHADER.CreateShader(
      ShaderSources.GetShaderSource("drawVertexSprite"),
      gl.VERTEX_SHADER);
   SPRITE_SHADER.CreateShader(
      ShaderSources.GetShaderSource("drawFragmentSprite"),
      gl.FRAGMENT_SHADER);
   SPRITE_SHADER.CreateProgram();

   INSTANCED_SPRITE_SHADER.InitGL(gl);
   INSTANCED_SPRITE_SHADER.CreateShader(
      ShaderSources.GetShaderSource("drawVertexInstancedSprite"),
      gl.VERTEX_SHADER);
   INSTANCED_SPRITE_SHADER.CreateShader(
      ShaderSources.GetShaderSource("drawFragmentSprite"),
      gl.FRAGMENT_SHADER);
   INSTANCED_SPRITE_SHADER.CreateProgram();


   //--------------------------------------------------------------------------
   // Setup textures.

   CAR_TEX.InitGL(gl);
   await CAR_TEX.LoadTexture("Textures/car.png");


   //--------------------------------------------------------------------------
   // Setup sprites and load map.

   PLAYER_CAR.InitGL(gl);
   PLAYER_CAR.SetTexture(CAR_TEX);

   await LoadMap("Maps/track1.txt");

   SetKeyText();

   StartRace();

   window.requestAnimationFrame(Update);
}


//--------------------------------------------------------------------------
// This function sets up the WebGL context.
//
function InitWebGL() {
   try {
      gl = CANVAS.getContext("webgl2", {alpha: true});
   }
   catch(e) {
   }

   //--------------------------------------------------------------------------
   // If we don't have a GL context, give up now

   if (!gl) {
      alert("Unable to initialize WebGL. Your browser may not support it.");
      return false;
   }

   return true;
}


//--------------------------------------------------------------------------
// This function is the main loop of the game. It will be called every frame.
//
function Update(currentFrame) {

   var dt = Math.min((currentFrame - PREVIOUS_FRAME) * 0.001, 0.016);
   PREVIOUS_FRAME = currentFrame;

   if (RACE_STARTED == false) {
      Draw();

      COUNTDOWN_TIMER -= dt;
      document.getElementById("Countdown").innerText = "Race Start In: " + COUNTDOWN_TIMER.toFixed(0);

      if (COUNTDOWN_TIMER < 0) {
         RACE_STARTED = true;
         document.getElementById("Countdown").style.opacity = 0;
         document.getElementById("KeyCountdown").style.opacity = 1;
         document.getElementById("NewKey").style.opacity = 1;
      }

      window.requestAnimationFrame(Update);

      return;
   }

   CHANGE_TIMER -= dt;
   FADE_OUT_TIMER -= dt;
   CURRENT_RACE_TIMER += dt;

   if (CHANGE_TIMER < 0) {
      RandomiseControls();
      CHANGE_TIMER = CHANGE_TIME;
   }

   HandleControls(dt);

   CheckCollision();

   Draw();

   var textTimer = CHANGE_TIMER.toFixed(1);
   document.getElementById("KeyCountdown").innerText = "Key Change In: " + textTimer;
   
   var fadeOut = (FADE_OUT_TIMER / FADE_OUT_TIME).toFixed(2);
   document.getElementById("NewKey").style.opacity = fadeOut;

   document.getElementById("CurrentTime").innerText = "Time: " + CURRENT_RACE_TIMER.toFixed(2);

   var numPassed = 0;

   for (var i = 0; i < TRACK.length; i++) {
      var xDiff = PLAYER_POSITION.x - TRACK[i].worldX;
      var yDiff = PLAYER_POSITION.y - TRACK[i].worldY;
   
      xDiff *= xDiff;
      yDiff *= yDiff;
   
      if (xDiff + yDiff < CHECKPOINT_DISTANCE * CHECKPOINT_DISTANCE)
         TRACK[i].passed = true;

      if (TRACK[i].passed) 
         numPassed++;
   }

   if (numPassed > 0.9 * TRACK.length) {
      var xDiff = PLAYER_POSITION.x - START_TILE.worldX;
      var yDiff = PLAYER_POSITION.y - START_TILE.worldY;
   
      xDiff *= xDiff;
      yDiff *= yDiff;
   
      if (xDiff + yDiff < CHECKPOINT_DISTANCE * CHECKPOINT_DISTANCE) {
         MarkLapComplete();
      }
   }

   window.requestAnimationFrame(Update);
}


//--------------------------------------------------------------------------
// This function is responsible for drawing everything.
//
function Draw() {

   //--------------------------------------------------------------------------
   // Clear the canvas before we start drawing on it.

   gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


   //--------------------------------------------------------------------------
   // Draw the instanced sprites.
   
   INSTANCED_SPRITE_SHADER.Use();

   INSTANCED_SPRITE_SHADER.UniformMatrix4fv("proj", CAMERA.orthoProj);
   INSTANCED_SPRITE_SHADER.UniformMatrix4fv("view", CAMERA.view);
   INSTANCED_SPRITE_SHADER.UniformMatrix4fv("model", Matrix4.Translate(0, 0, -0.1));

   for (var i = 0; i < MAP_SPRITES.length; i++)
   {
      MAP_SPRITES[i].Draw(INSTANCED_SPRITE_SHADER);
   }


   //--------------------------------------------------------------------------
   // Draw the non-instanced sprites.

   SPRITE_SHADER.Use();

   SPRITE_SHADER.UniformMatrix4fv("proj", CAMERA.orthoProj);
   SPRITE_SHADER.UniformMatrix4fv("view", new Matrix4());

   var playerOffset = Matrix4.Translate(
      -CAMERA.xOffset,
      -CAMERA.yOffset,
      -CAMERA.zOffset);

   SPRITE_SHADER.UniformMatrix4fv(
      "model", 
      Matrix4.Multiply(Matrix4.Rotate(0, 0, PLAYER_ANGLE), playerOffset));

   PLAYER_CAR.Draw(SPRITE_SHADER);

   SPRITE_SHADER.UniformMatrix4fv("view", CAMERA.view);
   SPRITE_SHADER.UniformMatrix4fv("model", Matrix4.Translate(0, 0, -0.1));
   //BACKGROUND.Draw(SPRITE_SHADER);
}


//--------------------------------------------------------------------------
// This function takes in user input that was mapped to the InputManager in
// the Start() function and applies it to the camera. Collision is also
// handled in here.
//
function HandleControls(dt) {

   PLAYER_ACCELERATION = 0;

   if (InputManager.IsKeyPressed(FORWARD_KEY))
   {
      PLAYER_ACCELERATION -= JERK;
   }
   else if (InputManager.IsKeyPressed(BACKWARD_KEY))
   {
      PLAYER_ACCELERATION += JERK;
   }

   if (InputManager.IsKeyPressed(LEFT_KEY))
   {
      PLAYER_ANGLE += ANGLE_SPEED * dt;

      PLAYER_DIRECTION.x = Math.cos(PLAYER_ANGLE);
      PLAYER_DIRECTION.y = Math.sin(PLAYER_ANGLE);
   }
   else if (InputManager.IsKeyPressed(RIGHT_KEY))
   {
      PLAYER_ANGLE -= ANGLE_SPEED * dt;

      PLAYER_DIRECTION.x = Math.cos(PLAYER_ANGLE);
      PLAYER_DIRECTION.y = Math.sin(PLAYER_ANGLE);
   }

   var playerTileX = Math.round(PLAYER_POSITION.x / 64);
   var playerTileY = Math.round(PLAYER_POSITION.y / 64);

   playerTileX = Clamp(playerTileX, 0, MAP.length - 1);
   playerTileY = Clamp(playerTileY, 0, MAP[0].length - 1);

   var tileID = MAP[playerTileX][playerTileY];
   var friction = FRICTION;

   for (var i = 0; i < MAP_TILES.length; i++)
   {
      if (MAP_TILES[i].id == tileID)
      {
         friction = MAP_TILES[i].friction;
         break;
      }
   }
   
   PLAYER_VELOCITY += PLAYER_ACCELERATION * dt;
   PLAYER_VELOCITY *= friction;

   PLAYER_VELOCITY = Clamp(
      PLAYER_VELOCITY,
      -MAX_VELOCITY,
      MAX_VELOCITY);

   PLAYER_POSITION = Vector3.Add(
      PLAYER_POSITION, 
      Vector3.Multiply(PLAYER_DIRECTION, PLAYER_VELOCITY * dt));

   CAMERA.view = Matrix4.Translate(
      -PLAYER_POSITION.x - CAMERA.xOffset,
      -PLAYER_POSITION.y - CAMERA.yOffset,
      -PLAYER_POSITION.z - CAMERA.zOffset);
}


function CheckCollision() {
   var playerTile = {
      x: Math.round(PLAYER_POSITION.x / 64),
      y: Math.round(PLAYER_POSITION.y / 64)};

   var startX = Clamp(playerTile.x - 1, 0, MAP.length);
   var startY = Clamp(playerTile.y - 1, 0, MAP[0].length);
   var endX = Clamp(playerTile.x + 1, 0, MAP.length);
   var endY = Clamp(playerTile.y + 1, 0, MAP[0].length);

   for (var x = startX; x < endX; x++) {
      for (var y = startY; y < endY; y++) {
         if (MAP[x][y] == 7) {
            var xDiff = x * 64 - PLAYER_POSITION.x;
            var yDiff = y * 64 - PLAYER_POSITION.y;

            xDiff *= xDiff;
            yDiff *= yDiff;

            if (xDiff + yDiff < CHECKPOINT_DISTANCE * CHECKPOINT_DISTANCE) {
               PLAYER_VELOCITY *= -1;
               return;
            }
         }
      }
   }
}


function SetKeyText() {
   document.getElementById("ForwardKey").innerText = "Forward: " + FORWARD_KEY;
   document.getElementById("BackwardKey").innerText = "Reverse: " + BACKWARD_KEY;
   document.getElementById("LeftKey").innerText = "TurnLeft: " + LEFT_KEY;
   document.getElementById("RightKey").innerText = "TurnRight: " + RIGHT_KEY;
}


function GetRandomNumber(numbers) {
   while (true) {
      var num = Math.round(Math.random() * (POSSIBLE_KEYS.length - 1));

      var unique = true;
      for (var i = 0; i < numbers.length; i++) {
         if (num == numbers[i]) {
            unique = false;
            break;
         }
      }

      if (unique) {
         return num;
      }
   }

}


function RandomiseControls() {

   var numbers = [
      FORWARD_KEY_INDEX,
      BACKWARD_KEY_INDEX,
      LEFT_KEY_INDEX,
      RIGHT_KEY_INDEX];
   
   var keyChange = Math.round(Math.random() * 3);
   var newKey = GetRandomNumber(numbers);

   if (keyChange == 0) {
      FORWARD_KEY = POSSIBLE_KEYS.charAt(newKey);
      FORWARD_KEY_INDEX = newKey;
      document.getElementById("NewKey").innerText = "NEW KEY -> Forward: " + FORWARD_KEY;
      FADE_OUT_TIMER = FADE_OUT_TIME;
   } else if (keyChange == 1) {
      BACKWARD_KEY = POSSIBLE_KEYS.charAt(newKey);
      BACKWARD_KEY_INDEX = newKey;
      document.getElementById("NewKey").innerText = "NEW KEY -> Reverse: " + BACKWARD_KEY;
      FADE_OUT_TIMER = FADE_OUT_TIME;
   } else if (keyChange == 2) {
      LEFT_KEY = POSSIBLE_KEYS.charAt(newKey);
      LEFT_KEY_INDEX = newKey;
      document.getElementById("NewKey").innerText = "NEW KEY -> Left: " + LEFT_KEY;
      FADE_OUT_TIMER = FADE_OUT_TIME;
   } else if (keyChange == 3) {
      RIGHT_KEY = POSSIBLE_KEYS.charAt(newKey);
      RIGHT_KEY_INDEX = newKey;
      document.getElementById("NewKey").innerText = "NEW KEY -> Right: " + RIGHT_KEY;
      FADE_OUT_TIMER = FADE_OUT_TIME;
   }

   SetKeyText();
}


function StartRace() {
   CURRENT_RACE_TIMER = 0;
   CURRENT_LAP = 0;
   document.getElementById("CurrentLap").innerText = "Lap: " + CURRENT_LAP + "\\" + MAX_LAPS;

   PLAYER_POSITION.x = START_TILE.worldX;
   PLAYER_POSITION.y = START_TILE.worldY;

   PLAYER_DIRECTION = new Vector3(
      Math.cos(PLAYER_ANGLE),
      Math.sin(PLAYER_ANGLE),
      0);

   CHANGE_TIMER = CHANGE_TIME;
   FADE_OUT_TIMER = 0;

   RACE_STARTED = false;

   COUNTDOWN_TIMER = 3;
   document.getElementById("Countdown").style.opacity = 1;
   document.getElementById("KeyCountdown").style.opacity = 0;
   document.getElementById("NewKey").style.opacity = 0;
   document.getElementById("WinText").style.opacity = 0;


   FORWARD_KEY = "W";
   BACKWARD_KEY = "S";
   LEFT_KEY = "A";
   RIGHT_KEY = "D";

   FORWARD_KEY_INDEX = 22;
   BACKWARD_KEY_INDEX = 18;
   LEFT_KEY_INDEX = 0;
   RIGHT_KEY_INDEX = 3;

   CAMERA.view = Matrix4.Translate(
      -PLAYER_POSITION.x - CAMERA.xOffset,
      -PLAYER_POSITION.y - CAMERA.yOffset,
      -PLAYER_POSITION.z - CAMERA.zOffset);

   SetKeyText();
}


function MarkLapComplete() {
   for (var i = 0; i < TRACK.length; i++) {
      TRACK[i].passed = false;
   }

   CURRENT_LAP++;
   document.getElementById("CurrentLap").innerText = "Lap: " + CURRENT_LAP + "\\" + MAX_LAPS;

   if (CURRENT_LAP == MAX_LAPS) {
      document.getElementById("WinText").style.opacity = 1;
      document.getElementById("WinText").innerText = "Race Complete! Time: " + CURRENT_RACE_TIMER.toFixed(2);
      
      if (CURRENT_RACE_TIMER < BEST_RACE_TIMER || BEST_RACE_TIMER == 0) {
         BEST_RACE_TIMER = CURRENT_RACE_TIMER;
         document.getElementById("BestTime").innerText = "Best Time: " + BEST_RACE_TIMER.toFixed(2);
      }
      
      setTimeout(StartRace, 3000);
   }
}


async function LoadMapTextures(filename) {
   var res = await fetch(filename);
   var text = await res.text();

   var lines = text.split('\n');

   for (var i = 0; i < lines.length; i++) {
      var tile = lines[i].split(',');

      var textureIndex = MAP_TEXTURES.length;
      MAP_TEXTURES[textureIndex] = new Texture();
      MAP_TEXTURES[textureIndex].InitGL(gl);
      MAP_TEXTURES[textureIndex].LoadTexture(tile[1]);

      var spriteIndex = MAP_SPRITES.length;
      MAP_SPRITES[spriteIndex] = new InstancedSprite(-32, -32, 64, 64);
      MAP_SPRITES[spriteIndex].InitGL(gl);
      MAP_SPRITES[spriteIndex].SetTexture(MAP_TEXTURES[textureIndex]);

      var mapTileIndex = MAP_TILES.length;
      MAP_TILES[mapTileIndex] = { 
         id: parseInt(tile[0]),
         spriteIndex: spriteIndex,
         friction: parseFloat(tile[2])};

      if (MAP_TILES[mapTileIndex].id == 0) {
         BACKGROUND = new Sprite(-32, -32, 64, 64);
         BACKGROUND.InitGL(gl);
         BACKGROUND.SetTexture(MAP_TEXTURES[textureIndex]);
      }
   }
}


async function LoadMap(filename) {
   await LoadMapTextures("Textures/textureList.txt");

   var res = await fetch(filename);
   var text = await res.text();

   var lines = text.split('\n');
   var tilesize = 64;
   var padding = 5;
   var mapSize = { 
      x: lines[0].split(',').length + 2 * padding,
      y: lines.length + 2 * padding };

   for (var x = 0; x < mapSize.x; x++) {
      MAP[x] = [];

      for (var y = 0; y < mapSize.y; y++) {
         MAP[x][y] = 0;

         if (x == 0 || x == mapSize.x - 1 || 
             y == 0 || y == mapSize.y - 1) {
            
            MAP[x][y] = 7;
         }
      }
   }

   for (var i = 0; i < lines.length; i++) {
      var row = lines[lines.length - 1 - i].split(',');

      for (var j = 0; j < row.length; j++) {
         MAP[j + padding][i + padding] = parseInt(row[j]);
      }
   }

   var positions = [];
   for (var i = 0; i < MAP_SPRITES.length; i++) {
      positions[i] = [];
   }

   for (var x = 0; x < MAP.length; x++) {
      for (var y = 0; y < MAP[0].length; y++) {
         for (var tileIndex = 0; tileIndex < MAP_TILES.length; tileIndex++) {

            if (MAP[x][y] == MAP_TILES[tileIndex].id) {
               if (MAP[x][y] == 2 && START_TILE.x == 0 && START_TILE.y == 0) {
                  START_TILE.x = x;
                  START_TILE.y = y;

                  START_TILE.worldX = x * tilesize;
                  START_TILE.worldY = y * tilesize;
               }

               if (MAP[x][y] > 0 && MAP[x][y] < 7) {
                  TRACK[TRACK.length] = {
                     x: x,
                     y: y,
                     worldX: x * tilesize,
                     worldY: y * tilesize,
                     passed: false};
               }

               var spriteIndex = MAP_TILES[tileIndex].spriteIndex;
               positions[spriteIndex].push(x * tilesize);
               positions[spriteIndex].push(y * tilesize);
               positions[spriteIndex].push(0);
               break;
            }
         }
      }
   }

   for (var i = 0; i < MAP_SPRITES.length; i++) {
      var spriteCount = Math.floor(positions[i].length / 3);
      MAP_SPRITES[i].SetPositions(positions[i], spriteCount);
   }

   BACKGROUND.SetSize(MAP.length * 64, MAP[0].length * 64);
   BACKGROUND.SetSize(MAP.length * 64, MAP[0].length * 64);
   BACKGROUND.SetTextureScale(MAP.length);
}



//--------------------------------------------------------------------------
// This function loads all the '.obj' models listed in the 'model-list.txt'
// file.
//
async function LoadAllModels() {
   //var res = await fetch("Models/model-list.txt");
   //var text = await res.text();

   //var lines = text.split('\n');

   //for (var i = 0; i < lines.length; i++) {
   //   var model = await LoadModel("Models/", lines[i]);
   //}
}


//--------------------------------------------------------------------------
// This function loads a specified '.obj' model and returns the vertex data
// neccessary to create either a Model() or an InstancedModel(). 
// 
// NOTE: I have only tested this with MagicaVoxel (https://ephtracy.github.io/)
//       exported '.obj' files. There are probably bugs if using a generic
//       '.obj' file.
//
async function LoadModel(dir, fileName) {
   var res = await fetch(dir + fileName);
   var text = await res.text();

   var lines = text.split('\n');

   var mtlFileName = "";

   var vertices = [];
   var normals = [];
   var texCoords = [];

   var vertexData = [];

   for (var i = 0; i < lines.length; i++) {
      var l = lines[i].split(' ');
      l[0] = l[0].trim();

      if (l[0] == "mtllib") {
         mtlFileName = l[1];

      } else if (l[0] == "vn") {
         normals[normals.length] = parseFloat(l[1]);
         normals[normals.length] = parseFloat(l[2]);
         normals[normals.length] = parseFloat(l[3]);

      } else if (l[0] == "vt") {
         texCoords[texCoords.length] = parseFloat(l[1]);
         texCoords[texCoords.length] = parseFloat(l[2]);

      } else if (l[0] == "v") {
         vertices[vertices.length] = parseFloat(l[1]);
         vertices[vertices.length] = parseFloat(l[2]);
         vertices[vertices.length] = parseFloat(l[3]);

      } else if (l[0] == "f") {
         for (var j = 1; j < 4; j++) {
            var v = l[j].split('/');

            var vertexIndex = parseInt(v[0]) - 1;
            var textureIndex = parseInt(v[1]) - 1;
            var normalIndex = parseInt(v[2]) - 1;

            vertexData[vertexData.length] = vertices[vertexIndex * 3 + 0];
            vertexData[vertexData.length] = vertices[vertexIndex * 3 + 1];
            vertexData[vertexData.length] = vertices[vertexIndex * 3 + 2];

            vertexData[vertexData.length] = texCoords[textureIndex * 2 + 0];
            vertexData[vertexData.length] = texCoords[textureIndex * 2 + 1];

            vertexData[vertexData.length] = normals[normalIndex * 3 + 0];
            vertexData[vertexData.length] = normals[normalIndex * 3 + 1];
            vertexData[vertexData.length] = normals[normalIndex * 3 + 2];
         }
      }
   }

   var texturePath = await LoadMTL(dir, mtlFileName);

   return {vertexData: vertexData, texturePath: texturePath};
}


//--------------------------------------------------------------------------
// This function loads a specified '.mtl' file associated with a '.obj' file.
// 
// NOTE: I have only tested this with MagicaVoxel (https://ephtracy.github.io/)
//       exported '.obj' files. There are probably bugs if using a generic
//       '.obj' file.
//
async function LoadMTL(dir, filePath) {
   var res = await fetch(dir + filePath);
   var text = await res.text();

   var lines = text.split('\n');

   var texFilePath = "";
   
   for (var i = 0; i < lines.length; i++) {
      var l = lines[i].split(' ');
      l[0] = l[0].trim();

      if (l[0] == "map_Kd") {
         texFilePath = l[1];
      }
   }

   return dir + texFilePath;
}


//--------------------------------------------------------------------------
// This function resizes the WebGL canvas whenever the browser window is
// also resized.
//
function ResizeCanvas() {
   CANVAS = document.getElementById("glcanvas");
   CANVAS.width = CANVAS.clientWidth;
   CANVAS.height = CANVAS.clientHeight;

   gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);

   CAMERA.proj = Matrix4.GetPerspective(Math.PI * 0.25, CANVAS.width / CANVAS.height, 0.1, 10000);
   CAMERA.orthoProj = Matrix4.GetOrthographic(0, CANVAS.width, 0, CANVAS.height, -1.0, 1.0);

   CAMERA.xOffset = -CANVAS.width * 0.5;
   CAMERA.yOffset = -CANVAS.height * 0.5;
}


//--------------------------------------------------------------------------
// This function clamps a value between a minimum value and a maximum.
//
function Clamp(t, min, max) {
   t = Math.max(t, min);
   t = Math.min(t, max);

   return t;
}


return {
   Start: Start,
}

})();