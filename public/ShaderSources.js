var ShaderSources = (function() {

   var SOURCES = {};

   SOURCES.drawVertexSprite = `#version 300 es
      precision mediump float;

      layout(location=0) in vec3 position;
      layout(location=1) in vec2 texCoords;

      uniform mat4 proj;
      uniform mat4 view;
      uniform mat4 model;

      out vec2 vTexCoords;

      void main(void) 
      {
         vec4 mPos = model * vec4(position, 1.0);
         gl_Position = proj * view * mPos;  

         vTexCoords = texCoords;
      }
   `;

   SOURCES.drawFragmentSprite = `#version 300 es
      precision mediump float;

      in vec2 vTexCoords;

      uniform sampler2D tex;

      out vec4 fragColour;
      void main(void) 
      {  
         fragColour = texture(tex, vTexCoords);

         if (fragColour.a == 0.0) discard;
      }
   `;

   SOURCES.drawVertexInstancedSprite = `#version 300 es
   precision mediump float;

   layout(location=0) in vec3 position;
   layout(location=1) in vec2 texCoords;
   layout(location=2) in vec3 offset;

   uniform mat4 proj;
   uniform mat4 view;
   uniform mat4 model;

   out vec2 vTexCoords;

   void main(void) 
   {
      vec4 mPos = model * vec4(position + offset, 1.0);
      gl_Position = proj * view * mPos;  

      vTexCoords = texCoords;
   }
`;

   SOURCES.drawVertexUI = `#version 300 es
      precision mediump float;

      layout(location=0) in vec3 position;
      layout(location=1) in vec2 texCoords;

      uniform mat4 proj;

      out vec3 vPos;
      out vec2 vTexCoords;

      void main(void) 
      {
         vec4 mPos = vec4(position, 1.0);
         gl_Position = proj * mPos;  

         vPos = mPos.xyz;
         vTexCoords = texCoords;
      }
   `;

   SOURCES.drawFragmentUI = `#version 300 es
      precision mediump float;

      in vec3 vPos;
      in vec2 vTexCoords;

      uniform vec3 colour;

      out vec4 fragColour;
      void main(void) 
      {  
         fragColour = vec4(colour, 1.0);
      }
   `;



   function GetShaderSource(name) {
         return SOURCES[name];
   }

   return {
      GetShaderSource: GetShaderSource,
   };

})();