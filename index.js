const express = require('express');
const app = express();
const http = require('http').Server(app);
var livereload = require('livereload');
const port = process.env.PORT || 3000;

// Setup the Express server
app.use(express.static(__dirname + '/public'));

// Setup the live reload
var lrserver = livereload.createServer({
  port: 35729,
});

lrserver.watch(__dirname + "/public");

// Open the port for listening
http.listen(port, () => {
  console.log('listening on *:3000');
});